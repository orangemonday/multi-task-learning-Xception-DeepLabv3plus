import cv2
import torch

from datasets import Vistas, utils

raw_dir = "Vistas/validation/images/"
instance_dir = "Vistas/validation/instances/"
semantic_dir = "Vistas/validation/labels"

trainset = Vistas(raw_dir, semantic_dir, instance_dir)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True, num_workers=4)

cv2.namedWindow("image", cv2.WINDOW_NORMAL)
cv2.namedWindow("semantic", cv2.WINDOW_NORMAL)
cv2.namedWindow('instance', cv2.WINDOW_NORMAL)

for j, data in enumerate(trainloader, 1):
    image = data[0].permute(0, 2, 3, 1).numpy()[0]
    semantic_gt = data[1].numpy()[0]
    instance_gt = data[2].permute(0, 2, 3, 1).numpy()[0]

    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    semantic_image = utils.color_semantic_label(semantic_gt)
    semantic_image = cv2.cvtColor(semantic_image, cv2.COLOR_RGB2BGR)

    instance_image = utils.color_disp_label(instance_gt)

    cv2.imshow("image", image)
    cv2.imshow("semantic", semantic_image)
    cv2.imshow("instance", instance_image)

    cv2.resizeWindow("image", 900,900)
    cv2.resizeWindow("semantic", 900,900)
    cv2.resizeWindow("instance", 900,900)

    cv2.waitKey()
    
