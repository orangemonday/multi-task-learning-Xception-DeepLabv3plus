import os
import torch
import numpy as np
import torchvision.transforms as transforms

from PIL import Image
from torch.utils.data import Dataset

from . import utils

#For Vistas dataset, the data is organized as follows:
#Each unique raw image [in Vistas_Raw]has a set of instance, semantic and panoptic ground truth label with each of them sharing the same file name
#Each of those images are stored respectively in their subfolders [Vistas_Semantic, Vistas_Instance, Vistas_Panoptic]
#(i.e. if the raw image file directory is /Users/vincentchooi/desktop/classifier/multi-task/multi-task/vistas/training/Vistas_Raw/M2kh294N9c72sICO990Uew.jpg)
#(then, the corresponding semantic label  /Users/vincentchooi/desktop/classifier/multi-task/multi-task/vistas/training/Vistas_Semantic/M2kh294N9c72sICO990Uew.jpg)
class Vistas(Dataset):
        def __init__(self, raw_dir, semantic_dir, instance_dir):

                self.input_size = (512, 512)
                self.output_size = (256, 256)
                #classes: (np ndarray) (K, 3) array of RGB values of K classes
                #raw_dir: (directory) Folder directory of raw input image files
                #instance_dir: (directory) Folder directory of instance ground truth
                #semantic_dir: (directory) Folder directory of semantic ground truth

                self.image_transform = transforms.Compose([
                    transforms.Resize(self.input_size),
                    transforms.ToTensor()
                ])

                self.gt_transform = transforms.Compose([
                    transforms.Resize(self.input_size, interpolation=Image.NEAREST)
                ])

                self.raw_dir = raw_dir
                self.semantic_dir = semantic_dir
                self.instance_dir = instance_dir
                self.list_img = os.listdir(self.raw_dir)

        def __len__(self):
                return len(self.list_img)

        def __getitem__(self, idx):

                image_name = self.list_img[idx]
                image_path = os.path.join(self.raw_dir, image_name)
                image_id = image_name.split('.')[0]
                image = Image.open(image_path)
                image_array = np.array(image, dtype=np.uint8)

                image = self.image_transform(image)

                instance_path = "{}/{}.png".format(self.instance_dir, image_id)
                instance_image = Image.open(instance_path)
                instance_image = self.gt_transform(instance_image)
                instance_array = np.array(instance_image, dtype=np.uint16)

                semantic_gt, disp_gt = utils.vistas_inst_to_gt(instance_array)

                semantic_gt = torch.from_numpy(semantic_gt).long()
                disp_gt = torch.from_numpy(disp_gt).float()
                disp_gt = disp_gt.permute(2, 0, 1)

                data = (image, semantic_gt, disp_gt)

                return data

