import cv2
import numpy as np
import json

from skimage.measure import regionprops

config_file = "./config.json"
with open(config_file) as f:
    config = json.load(f)
    labels = config['labels']

# extract instance only from selected categories based on config.json
def apply_instance_filter(inst_label, semantic_label):
    for label_id, label in enumerate(labels):
        if not label["instances"]:
            inst_label[semantic_label == label_id] = 0

    return inst_label

def instance_to_disp_label(inst_label):
    H, W = inst_label.shape
    disp_label = np.zeros((H, W, 2), dtype=np.int32)

    centroids = []
    for i in regionprops(inst_label):
        cy, cx = map(lambda x: int(x), i.centroid)
        centroids.append((cx, cy))
        for y, x in i.coords:
            disp_label[y, x, 0] = cx - x
            disp_label[y, x, 1] = cy - y

    return disp_label

def vistas_inst_to_gt(inst_array):
    instance_label = np.array(inst_array % 256, dtype=np.uint8)
    semantic_label = np.array(inst_array / 256, dtype=np.uint8)

    instance_label = apply_instance_filter(instance_label, semantic_label)
    disp_label = instance_to_disp_label(instance_label)

    return semantic_label, disp_label

# disp_label: (H, W, 2)
def color_disp_label(disp_label):
    H, W, _ = disp_label.shape
    color_map = np.zeros((H, W, 3), dtype=np.uint8)

    deg = np.arctan2(disp_label[:, :, 1], disp_label[:, :, 0]) * 180 / np.pi
    mag = np.sqrt(disp_label[:,:,0]**2 + disp_label[:,:,1]**2)

    # hue
    hue = (deg + 180) / 360 * 255
    # saturation
    sat = (mag - np.min(mag)) / (np.max(mag) - np.min(mag)) * 255

    color_map[:,:,0] = hue
    color_map[:,:,1] = sat
    color_map[:,:,2] = 255
    color_map = cv2.cvtColor(color_map, cv2.COLOR_HSV2RGB)

    return color_map

# semantic_array: Tensor (H, W)
# Returns:
#   color_map: Array (H, W, C)
def color_semantic_label(semantic_label):
    H, W = semantic_label.shape
    color_map = np.zeros((H, W, 3), dtype=np.uint8)

    for label_id, label in enumerate(labels):
        color_map[semantic_label == label_id] = label['color']

    return color_map

