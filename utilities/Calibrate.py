import cv2
import numpy as np
import glob
import os
import time

'''
NOTE:
The function requires white space (like a square-thick border, the wider the better) 
around the board to make the detection more robust in various environments. 
Otherwise, if there is no border and the background is dark, 
the outer black squares cannot be segmented properly and 
so the square grouping and ordering algorithm fails.
Best to use provided image of chessboard, for asymmetry and non-ambiguity.
'''
class Calibrate():

    '''
    Calibrate.Calibrate.calibWithImg(path, size)
    Returns:
    rms: Overall RMS re-projection error
    mtx: Camera matrix containing intrinsic parameters of the camera, i.e. (focal length, optical centers)
    dist: Distortion coefficient vector
    rvecs: 3D rotation vector for each image/frame 
    tvecs: Corresponding 3D translation vector for each image/frame

    Parameters:
    path: The absolute path of the folder that holds all the images (.jpg format)
    size: The tuple (nrow, ncol) of the no. of black corners of the chessboard
    sq_size: Length/height of the squares on the chessboard (in mm)

    Note: The algorithm assumes that a flat chessboard will be used for calibration.
          Thus, the real-world object points of the corners are (X, Y, Z), s.t. Z = 0 in all cases, since it is flat.
          Like so, objpoints = [(0, 30, 0), (0, 60, 0), (0, 90, 0)...], for a chessboard with 30mm sized squares. 
          According to the paper, around 10-40 frames with good corners are required for good calibration.
    '''
    def calibWithImg(path, size, sq_size):

        #The criteria tuple:
        #EPS: Accuracy at which the algorithm stops
        #MAX_ITER: Maximum allowed iteration of the algorithm
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        objpoints = [] #3D points in real world space
        imgpoints = [] #2D points on image plane
        objp = Calibrate.getObjP(size, sq_size)
        images = glob.glob(os.path.join(path, '*.jpg'))

        for fname in images:
            img = cv2.imread(fname)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            found, corners = cv2.findChessboardCorners(img, size)

            if found:
                objpoints.append(objp)

                #Refine corners via gradient
                corners = cv2.cornerSubPix(img, corners, (11,11), (-1,-1), criteria)
                imgpoints.append(corners)

        #img.shape[::-1] to reverse (H, W) to (W, H) shape type
        rms, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape[::-1], None, None)

        return (rms, mtx, dist, rvecs, tvecs)
    '''
    Calibrate.Calibrate.calibWithVid(path, size)
    Returns:
    rms: Overall RMS re-projection error
    mtx: Camera matrix containing intrinsic parameters of the camera, i.e. (focal length, optical centers)
    dist: Distortion coefficient vector
    rvecs: 3D rotation vector for each image/frame 
    tvecs: Corresponding 3D translation vector for each image/frame

    Parameters:
    path: Absolute path of the video file 
    size: Tuple (nrow, ncol) containing the no. of black corners of the chessboard
    sq_size: Length/height of the squares on the chessboard (in mm)
    max_frame: No. of good frames (between 10 - 40 is recommended)
    '''
    def calibWithVid(path, size, sq_size, max_frame):

        #The criteria tuple:
        #EPS: Accuracy at which the algorithm stops
        #MAX_ITER: Maximum allowed iteration of the algorithm
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        objpoints = [] #3D points in real world space
        imgpoints = [] #2D points on image plane
        objp = Calibrate.getObjP(size, sq_size)

        cap = cv2.VideoCapture(path)
        no_frame = 0

        while(cap.isOpened() and no_frame < max_frame and ret):
            ret, frame = cap.read()
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            found, corners = cv2.findChessboardCorners(img, size)

            if found:
                objpoints.append(objp)

                #Refine corners via gradient
                corners = cv2.cornerSubPix(img, corners, (11,11), (-1,-1), criteria)
                imgpoints.append(corners)

                #Sets frame to 1.5 seconds later (for variation of chessboard orientation relative to camera)
                cap.set(cv2.CAP_PROP_POS_MSEC, cv2.CAP_PROP_POS_MSEC + 1500)
                no_frame += 1

        #img.shape[::-1] to reverse (H, W) to (W, H) shape type
        rms, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape[::-1], None, None)
        cap.release()

        return rms, mtx, dist, rvecs, tvecs

    '''
    Calibrate.Calibrate.getObjP(size, step)
    Returns:
    objp: Object point vector of chessboard size, objpoints = [(0, 30, 0), (0, 60, 0), (0, 90, 0)...] (for step = 30mm)

    Parameters:
    size: Tuple (nrow, ncol) containing the no. of black corners of the chessboard
    step: Length/height of the squares on the chessboard (in mm)
    '''
    def getObjP(size, step):
        objp = np.zeros((size[0]*size[1], 3), np.float32)
        objp[:, :2] = np.mgrid[0:size[0],0:size[1]].T.reshape(-1,2)
        objp *= step

        return objp

    '''
    Calibrate.Calibrate.getRelativeMat(Rw1, Tw1, Rw2, Tw2)
    Returns:
    R12: The 3x3 rotation matrix of camera 1 with respect to camera 2
    T12: The 1x3 translation vector of camera 1 with respect to camera 2

    Parameters:
    Rw1: List of 1x3 rotation vectors of the world coordinates to camera 1
    Tw1: List 1x3 translation vectors of the world coordinates to camera 1
    Rw2: List 1x3 rotation vectors of the world coordinates to camera 2
    Tw2: List 1x3 translation vectors of the world coordinates to camera 2
    '''
    def getRelativeMat(Rw1, Tw1, Rw2, Tw2, mat=False):

        Rw1_mat = list()
        Rw2_mat = list()

        #Convert rotation vectors to rotation matrix via Rodrigues' algorithm
        for i in range(len(Rw1)):
            dst, jacobian = cv2.Rodrigues(Rw1[i])
            Rw1_mat.append(dst)

        for i in range(len(Rw2)):
            dst, jacobian = cv2.Rodrigues(Rw2[i])
            Rw2_mat.append(dst)
        #Average over all reference rotation and translation vectors
        Rw1_mat_mean = np.mean(np.asarray(Rw1_mat), axis=0)
        Rw2_mat_mean = np.mean(np.asarray(Rw2_mat), axis=0)
        Tw1_mean = np.mean(np.asarray(Tw1), axis=0)
        Tw2_mean = np.mean(np.asarray(Tw2), axis=0)

        #Append R, T into a 4x4 roto-translation matrix, Q, with bottom row [0,0,0,1]
        toappend = np.array([0,0,0,1]).reshape(-1,4)
        Qw1 = np.append(Rw1_mat_mean, Tw1_mean, axis=1)
        Qw1 = np.append(Qw1, toappend, axis=0)
        Qw2 = np.append(Rw2_mat_mean, Tw2_mean, axis=1)
        Qw2 = np.append(Qw2, toappend, axis=0)

        #Inverse Qw1 -> Q1w, then Q1w * Qw2 = Q12
        Qw1_inv = np.linalg.inv(Qw1)
        Q12 = np.dot(Qw1_inv, Qw2)

        if mat:
            return Q12
        else:
            #Extract R12 (3x3), T12(3x1) from Q
            R12 = Q12[:3, :3]
            T12 = Q12[:3, -1].reshape(3, -1)

            return R12, T12
