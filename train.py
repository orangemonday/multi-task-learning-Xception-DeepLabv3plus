import os
import argparse
import torch
import torchvision.utils as vutils
import torch.nn.functional as F
import numpy as np

from tqdm import tqdm
from tensorboardX import SummaryWriter

from datasets import Vistas, utils
from trainer import Trainer

batch_size = 3
num_epochs = 100

raw_dir = "Vistas/validation/images/"
instance_dir = "Vistas/validation/instances/"
semantic_dir = "Vistas/validation/labels"
weight_dir = "./weights"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

trainset = Vistas(raw_dir, semantic_dir, instance_dir)
dataloader = torch.utils.data.DataLoader(
                trainset, batch_size=batch_size,
                shuffle=True, num_workers=4
             )

trainer = Trainer(device)
writer = SummaryWriter()

def log_img(image, semantic, it):
    input_img = vutils.make_grid(image)
    input_array = input_img.to('cpu').numpy()

    semantic = F.softmax(semantic, dim=1)
    semantic = torch.argmax(semantic, dim=1, keepdim=True)
    semantic = vutils.make_grid(semantic)
    semantic = semantic.permute(1, 2, 0)
    semantic_array = semantic.to('cpu').numpy()[:,:,0]
    semantic_img = utils.color_semantic_label(semantic_array)
    semantic_img = np.transpose(semantic_img, (2, 0, 1))

    writer.add_image('input', input_array, it)
    writer.add_image('semantic', semantic_img, it)

def main(args):
    if args.resume:
        assert os.path.isfile(args.resume), \
            "%s is not a file" % args.resume

        checkpoint = torch.load(args.resume)
        trainer.load(checkpoint)
        it = checkpoint["iterations"] + 1

        print("Loaded checkpoint '{}' (iterations {})"
            .format(args.resume, checkpoint['iterations']))
    else:
        it = 1

    for e in range(1, num_epochs):
        print("Epoch %d" % e)

        sum_loss = 0
        t = tqdm(dataloader)
        for i, data in enumerate(t):
            # Input
            image = data[0].to(device)
            semantic_gt = data[1].to(device)
            instance_gt = data[2].to(device)

            # Train
            output = trainer.update(image, semantic_gt, instance_gt)

            # Log
            metrics = trainer.get_metrics()
            for k, v in metrics.items():
                writer.add_scalar(k, v, it)

            #if it % 500 == 0:
            log_img(image, output, it)

            t.set_postfix(loss=metrics['loss/total_loss']/batch_size)

            sum_loss += metrics['loss/total_loss']

            # Save
            if it % 1000 == 0:
                print('Saving checkpoint...')
                trainer.save(weight_dir, it)


            it += 1

        print('Average loss @ epoch: {}'.format((sum_loss / i * batch_size)))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--resume", help="Load weight from file")

    args = parser.parse_args()

    main(args)

