from Inference import Feed
from PIL import Image
from MultiTaskNetwork import MultiTaskNetwork
import numpy as np 
import torchvision.transforms as transforms

image = Image.open("./Vistas/validation/images/03G8WHFnNfiJR-457i0MWQ.jpg")
image_array = np.array(image, dtype=np.uint8)
input_size = (512,512)
image_transform = transforms.Compose([transforms.Resize(input_size), transforms.ToTensor()])

image = image_transform(image)
image = image.unsqueeze(0)

feed = Feed()
feed.load_weights("./multitask_weights.tar")

semantic, instance = feed(image)

semantic_image = Image.fromarray(semantic, mode="RGB")
instance_image = Image.fromarray(instance, mode="RGB")

semantic_image.save("./Model_Out/M2kh294N9c72sICO990Uew_S.jpg")
instance_image.save("./Model_Out/M2kh294N9c72sICO990Uew_I.jpg")

