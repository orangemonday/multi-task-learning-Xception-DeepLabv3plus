import torch
import torch.nn as nn
import torch.nn.functional as f
import torch.optim as optim

from os.path import join

from Deeplab_Xception import DeepLabv3plus

num_class = 66
lr = 1e-4
momentum = 0.9

class Trainer(nn.Module):
    def __init__(self, device):
        super(Trainer, self).__init__()

        self.model = DeepLabv3plus(num_class).to(device)
        #self.task_weights = nn.Parameter(torch.tensor([1.0, 1.0]).to(device))
        self.optim = optim.SGD([
                        {'params': self.model.parameters()},
                        #{'params': self.task_weights, 'lr': lr}
                     ], lr=lr, momentum=momentum)
        #self.mse_loss = nn.MSELoss()
        self.xent_crit = nn.CrossEntropyLoss(ignore_index=65)
        self.metrics = {}


    def update(self, image, semantic_gt, instance_gt):
        y = self.model(image)

        l2_loss = self.xent_crit(y, semantic_gt)

        total_loss = l2_loss

        #total_loss = (1 / (task_weights[0] ** 2)) * l2 + (1 / (2*(task_weights[1]**2))) * l1 + torch.log(task_weights[0]) + torch.log(task_weights[1])

        self.model.zero_grad()
        total_loss.backward()
        self.optim.step()

        metrics = {
            "loss/total_loss": total_loss.item(),
            "loss/semantic_loss": l2_loss.item(),
            #"loss/instance_loss": l1.item(),
            #"weight/semantic_weight": task_weights[0].item(),
            #"weight/instance_weight": task_weights[1].item()
        }
        self.metrics = metrics

        return y

    def get_metrics(self):
        return self.metrics

    def load(self, checkpoint):
        self.model.load_state_dict(checkpoint['weight'])
        #self.optim.load_state_dict(checkpoint['optim'])

    def save(self, save_dir, iterations):
        weight_fn = join(save_dir, "model_%d.pkl" % iterations)

        state = {
            'weight': self.model.state_dict(),
            #'task_weights': task_weights,
            'optim': self.optim.state_dict(),
            'iterations': iterations,
        }

        torch.save(state, weight_fn)

