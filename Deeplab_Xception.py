import torch
import torch.nn as nn
import torch.nn.functional as F
from xception import Xception, SeparableConv2d_same

class ASPP(nn.Module):
    def __init__(self, in_chn, out_chn, kernel_sizes, rates, batchnorm=False, relu=False):
        super(ASPP, self).__init__()
        
        self.aspp = nn.ModuleList()

        for i in range(len(kernel_sizes)):
            self.aspp.append(SeparableConv2d_same(
                in_chn,
                out_chn,
                kernel_size=kernel_sizes[i],
                stride=1,
                dilation=rates[i],
                batchnorm=batchnorm,
                relu=relu))

        # Global average pooling
        layers = []
        layers += [nn.AdaptiveAvgPool2d((1,1))]
        layers += [nn.Conv2d(in_chn, out_chn, kernel_size=1, stride=1)]
        layers += [nn.BatchNorm2d(out_chn)]
        self.global_features = nn.Sequential(*layers)

        last_in_chn = out_chn * (len(rates) + 1)
        self.last_conv = nn.Conv2d(last_in_chn, out_chn, kernel_size=1, stride=1)

    def forward(self, x):
        outs = []
        for i in range(len(self.aspp)):
            outs.append(self.aspp[i](x))
        
        feats = self.global_features(x)
        feats = F.interpolate(feats,
                    size=outs[0].size()[2:],
                    mode='bilinear',
                    align_corners=True)
        outs.append(feats)

        out = self.last_conv(torch.cat(outs, dim=1))

        return out

class RefineDecoder(nn.Module):
    def __init__(self, low_feats, in_chn, depth, num_conv, batchnorm=False, relu=False):
        super(RefineDecoder, self).__init__()

        self.low_feats = nn.Conv2d(low_feats, 48, kernel_size=1, stride=1)

        layers = []
        for i in range(num_conv):
            layers += [SeparableConv2d_same(
                            in_chn + 48 if i == 0 else depth, 
                            depth,
                            kernel_size=3,
                            stride=1,
                            dilation=1,
                            batchnorm=batchnorm,
                            relu=relu)]
        self.decoder_conv = nn.Sequential(*layers)

    def forward(self, x, low_feats):
        low_feats = self.low_feats(low_feats)

        x = F.interpolate(
                x,
                scale_factor=4,
                mode='bilinear',
                align_corners=True
            )

        x = torch.cat((x, low_feats), dim=1)

        x = self.decoder_conv(x)

        return x


class DeepLabv3plus(nn.Module):
    def __init__(self, num_class):
        super(DeepLabv3plus, self).__init__()

        depth = 256

        self.encoder = Xception(batchnorm=True, relu=True)

        self.aspp = ASPP(
                        2048,
                        256,
                        kernel_sizes=[1, 3, 3, 3],
                        rates=[1, 6, 12, 18]
                    )

        self.decoder = RefineDecoder(128, 256, depth, 2, batchnorm=True, relu=True)

        self.semantic_conv = nn.Conv2d(depth, num_class, kernel_size=1, stride=1)

    def forward(self, x):
        x, low_feats = self.encoder(x)

        x = self.aspp(x)

        x = self.decoder(x, low_feats)

        # get classification
        x = self.semantic_conv(x)

        # upsample
        x = F.interpolate(
                x,
                scale_factor=4,
                mode='bilinear',
                align_corners=True
            )

        return x

