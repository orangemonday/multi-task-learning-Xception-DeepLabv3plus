from MultiTaskNetwork import MultiTaskNetwork
from Devectorize import Devectorize
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import json
import os

class Feed():
	
	def __init__(self):
		self.model = MultiTaskNetwork()

	def load_weights(self, path):

		if os.path.isfile(path):
			print("Loading checkpoint '{}'".format(path))
			checkpoint = torch.load(path, map_location='cpu')
			self.epoch = checkpoint['epoch']
			self.model.load_state_dict(checkpoint['state_dict'])
			print("Loaded checkpoint '{}' (epoch {})".format(path, checkpoint['epoch']))
		else:
			print("No checkpoint found at '{}'".format(path))


	def __call__(self, image):
		self.model.eval()
		semantic, instance = self.model(image)
		semantic = F.log_softmax(semantic, dim=1) #Softmax along dimension C
		semantic = semantic.permute(0, 2, 3, 1) #Convert from (N, C, H, W) to (N, H, W, C)
		instance = instance.permute(0, 2, 3, 1)
		semantic = semantic.detach().numpy()
		instance = instance.detach().numpy()
		semantic = np.argmax(semantic, axis=3) #Max along dimension C, which is now axis no. 3
		semantic_out = Devectorize.apply_semantic_filter(np.squeeze(semantic))
		instance_out = Devectorize.get_color_map(np.squeeze(instance))

		return semantic_out, instance_out

