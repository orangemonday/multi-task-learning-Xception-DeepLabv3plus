import numpy as np 

#Both arrays have to be in the same size of (H, W) where each pixel is labelled 
#with a unique class label of N classes such that p_h_w = 0 ... N-1
def getIoU(array_a, array_b, no_classes, mean_iou=False):
	union = np.zeros((no_classes, array_a.shape[0], array_a.shape[1]), dtype=np.uint8)
	intersection = np.zeros((no_classes, array_a.shape[0], array_a.shape[1]), dtype=np.uint8)
	channels_a = np.zeros((no_classes, array_a.shape[0], array_a.shape[1]), dtype=np.uint8)
	channels_b = np.zeros((no_classes, array_a.shape[0], array_a.shape[1]), dtype=np.uint8)
	IoU = np.zeros((no_classes), dtype=np.float32)

	for i in range(no_classes):
		channels_a[i][array_a == i] = 1
		channels_b[i][array_b == i] = 1
		union[i][channels_a[i] == 1] = 1
		union[i][channels_b[i] == 1] = 1 
		intersection[i][(channels_a[i] + channels_b[i]) == 2] = 1
		IoU[i] = intersection[i].sum()/union[i].sum()

	if mean_iou == True:
		return ((IoU.sum()/no_classes), IoU)
	else:
		return IoU